package com.example.answersboxapi.question.service.impl;

import com.example.answersboxapi.generated.model.Question;
import com.example.answersboxapi.generated.model.QuestionDetails;
import com.example.answersboxapi.generated.model.Tag;
import com.example.answersboxapi.question.details.QuestionDetailsEntity;
import com.example.answersboxapi.question.details.QuestionDetailsRepository;
import com.example.answersboxapi.question.service.QuestionDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

import static com.example.answersboxapi.question.details.QuestionDetailsMapper.QUESTION_DETAILS_MAPPER;
import static com.example.answersboxapi.question.mapper.QuestionMapper.QUESTION_MAPPER;
import static com.example.answersboxapi.tag.mapper.TagMapper.TAG_MAPPER;

@Service
@RequiredArgsConstructor
public class QuestionDetailsServiceImpl implements QuestionDetailsService {

    private final QuestionDetailsRepository questionDetailsRepository;

    @Override
    public QuestionDetails create(final Question question, final Tag tag) {
        final QuestionDetailsEntity questionDetailsToSave = QuestionDetailsEntity.builder()
                .questionId(QUESTION_MAPPER.toEntity(question))
                .tagId(TAG_MAPPER.toEntity(tag))
                .build();

        return QUESTION_DETAILS_MAPPER.toModel(questionDetailsRepository.saveAndFlush(questionDetailsToSave));
    }

    @Override
    public List<QuestionDetails> getAllByQuestionId(final UUID id) {
        return QUESTION_DETAILS_MAPPER.toModelList(
                questionDetailsRepository.findAllByQuestionId(id));
    }

    @Override
    @Transactional
    public void deleteById(final UUID id) {
        questionDetailsRepository.deleteById(id);
    }
}
