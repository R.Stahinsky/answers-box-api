package com.example.answersboxapi.question.service;


import com.example.answersboxapi.generated.model.Question;
import com.example.answersboxapi.generated.model.QuestionDetails;
import com.example.answersboxapi.generated.model.Tag;

import java.util.List;
import java.util.UUID;

public interface QuestionDetailsService {

    QuestionDetails create(final Question question, final Tag tag);

    List<QuestionDetails> getAllByQuestionId(final UUID questionId);

    void deleteById(final UUID id);
}
