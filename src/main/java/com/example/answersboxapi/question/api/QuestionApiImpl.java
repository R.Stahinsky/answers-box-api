package com.example.answersboxapi.question.api;

import com.example.answersboxapi.generated.api.QuestionsApi;
import com.example.answersboxapi.generated.model.Answer;
import com.example.answersboxapi.generated.model.Question;
import com.example.answersboxapi.generated.model.QuestionRequest;
import com.example.answersboxapi.generated.model.QuestionUpdateRequest;
import com.example.answersboxapi.utils.SortParams;
import com.example.answersboxapi.question.service.QuestionService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import static com.example.answersboxapi.utils.pagination.HeaderUtils.generateHeaders;

@RestController
@Api(tags = "questions")
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class QuestionApiImpl implements QuestionsApi {

    private final QuestionService questionService;

    @Override
    public ResponseEntity<Question> createQuestion(@RequestBody final QuestionRequest questionRequest) {
        return new ResponseEntity<>(questionService.create(questionRequest), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<Question>> getAllQuestions(final List<UUID> tagIds, final Integer page, final Integer size,
                                                          final String sortParams, final String searchParam,
                                                          final Boolean isDeleted) {
        final Page<Question> foundQuestions = questionService.getAll(page, size, tagIds, SortParams.valueOf(sortParams), searchParam, isDeleted);
        final MultiValueMap<String, String> headers = generateHeaders(foundQuestions);

        return new ResponseEntity<>(foundQuestions.getContent(), headers, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Answer>> getAnswersByQuestionId(@PathVariable final UUID id, final Integer page, final Integer size,
                                                               final String sortParams, final String searchParam,
                                                               final Boolean isDeleted) {
        final Page<Answer> answers = questionService.getAnswersByQuestionId(id, page, size, SortParams.valueOf(sortParams), searchParam, isDeleted);
        final MultiValueMap<String, String> headers = generateHeaders(answers);

        return new ResponseEntity<>(answers.getContent(), headers, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Question> addTagToQuestion(@PathVariable final UUID questionId, @PathVariable final UUID tagId) {
        return new ResponseEntity<>(questionService.addTagToQuestion(questionId, tagId), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Question> removeTagFromQuestion(@PathVariable final UUID questionId, @PathVariable final UUID tagId) {
        return new ResponseEntity<>(questionService.removeTagFromQuestion(questionId, tagId), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Question> updateQuestion(@PathVariable final UUID id, @RequestBody final QuestionUpdateRequest questionUpdateRequest) {
        return new ResponseEntity<>(questionService.updateById(id, questionUpdateRequest), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteQuestionById(@PathVariable final UUID id) {
        questionService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<Question> increaseRating(@PathVariable final UUID id) {
        return new ResponseEntity<>(questionService.increaseRatingById(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Question> decreaseRating(@PathVariable final UUID id) {
        return new ResponseEntity<>(questionService.decreaseRatingById(id), HttpStatus.OK);
    }
}
