package com.example.answersboxapi.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@AllArgsConstructor
public class RedirectController {

    @GetMapping("/")
    public String defaultRedirect() {
        return "redirect:/swagger-ui.html#";
    }
}
