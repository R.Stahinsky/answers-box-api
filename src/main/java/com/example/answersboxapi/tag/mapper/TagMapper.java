package com.example.answersboxapi.tag.mapper;

import com.example.answersboxapi.generated.model.Tag;
import com.example.answersboxapi.tag.entity.TagEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TagMapper {

    TagMapper TAG_MAPPER = Mappers.getMapper(TagMapper.class);

    @Mapping(target = "questionDetails", ignore = true)
    TagEntity toEntity(final Tag tag);

    Tag toModel(final TagEntity tagEntity);
}
