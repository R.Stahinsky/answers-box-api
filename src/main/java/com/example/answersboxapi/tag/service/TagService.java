package com.example.answersboxapi.tag.service;


import com.example.answersboxapi.generated.model.Tag;
import com.example.answersboxapi.generated.model.TagRequest;

import java.util.UUID;

public interface TagService {

    Tag create(final TagRequest tagRequest);

    Tag getById(final UUID id);

    boolean existsById(final UUID id);

    void deleteById(final UUID id);
}
