package com.example.answersboxapi.tag.api;

import com.example.answersboxapi.generated.api.TagsApi;
import com.example.answersboxapi.generated.model.Tag;
import com.example.answersboxapi.generated.model.TagRequest;
import com.example.answersboxapi.tag.service.TagService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@Api(tags = "tags")
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class TagApiImpl implements TagsApi {

    private final TagService tagService;

    @Override
    public ResponseEntity<Tag> create(@RequestBody final TagRequest tagRequest) {
        return new ResponseEntity<>(tagService.create(tagRequest), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Tag> deleteTagById(@PathVariable final UUID id) {
        tagService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
