package com.example.answersboxapi.config;

import io.sentry.Sentry;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class SentryConfig {

    @Value("${sentry.dsn}")
    private String dsn;

    @Bean
    public void init(){
        Sentry.init(dsn);
    }
}
