package com.example.answersboxapi.user.mapper;

import com.example.answersboxapi.generated.model.User;
import com.example.answersboxapi.question.entity.QuestionEntity;
import com.example.answersboxapi.user.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ValueMapping;
import org.mapstruct.ValueMappings;
import org.mapstruct.factory.Mappers;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Mapper
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "questions", ignore = true)
    @Mapping(target = "answers", ignore = true)
    @ValueMappings({
            @ValueMapping(source = "ADMIN", target = "ROLE_ADMIN"),
            @ValueMapping(source = "USER", target = "ROLE_USER")})
    UserEntity toEntity(final User user);

    @Mapping(target = "questions", expression = "java(questionsToIds(userEntity.getQuestions()))")
    @ValueMappings({
            @ValueMapping(source = "ROLE_ADMIN", target = "ADMIN"),
            @ValueMapping(source = "ROLE_USER", target = "USER")
    })
    User toModel(final UserEntity userEntity);

    default List<UUID> questionsToIds(final List<QuestionEntity> questions) {
        if (questions != null) {
            return questions.stream().map(QuestionEntity::getId).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    List<User> toModelList(final List<UserEntity> answersList);
}
