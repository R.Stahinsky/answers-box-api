package com.example.answersboxapi.user.api;

import com.example.answersboxapi.generated.api.UsersApi;
import com.example.answersboxapi.generated.model.ChangePasswordRequest;
import com.example.answersboxapi.generated.model.User;
import com.example.answersboxapi.user.service.UserService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@Api(tags = "users")
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class UserApiImpl implements UsersApi {

    private final UserService userService;

    @Override
    public ResponseEntity<Void> deleteById(@PathVariable final UUID id) {
        userService.deleteById(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

    @Override
    public ResponseEntity<Void> changePassword(@RequestBody final ChangePasswordRequest changePasswordRequest) {
        userService.changePassword(changePasswordRequest);
        return new ResponseEntity<>(OK);
    }

    @Override
    public ResponseEntity<List<User>> getAll(@RequestParam final Boolean isEnabled, @RequestParam final Boolean isDeleted) {
        return new ResponseEntity<>(userService.getAll(isEnabled, isDeleted), OK);
    }
}
