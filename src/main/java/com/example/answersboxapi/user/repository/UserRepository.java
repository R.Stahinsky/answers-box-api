package com.example.answersboxapi.user.repository;

import com.example.answersboxapi.user.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    @Query(value = "SELECT EXISTS(SELECT * FROM users WHERE email = :email AND is_enabled IS TRUE)", nativeQuery = true)
    boolean existsByEmail(@Param("email") final String email);

    @Query(value = "SELECT EXISTS(SELECT * FROM users WHERE id = :id AND deleted_at IS NULL AND is_enabled IS TRUE)", nativeQuery = true)
    boolean existsById(@Param("id") final UUID id);

    @Query(value = "SELECT * FROM users WHERE email = :email AND deleted_at IS NULL AND is_enabled = :isEnabled", nativeQuery = true)
    Optional<UserEntity> findByEmail(@Param("email") final String email, @Param("isEnabled") final boolean isEnabled);

    @Modifying
    @Query(value = "UPDATE users SET deleted_at = NOW() WHERE id = :id ;", nativeQuery = true)
    void deleteById(@Param("id") final UUID id);

    @Query(value = "SELECT * FROM users WHERE reset_password_token = :resetPasswordToken AND deleted_at IS NULL", nativeQuery = true)
    Optional<UserEntity> findByPasswordResetToken(@Param("resetPasswordToken") final String resetPasswordToken);

    @Query(value = "SELECT * FROM users " +
            "WHERE is_enabled = :isEnabled " +
            "AND (:isDeleted IS NULL OR :isDeleted = TRUE AND deleted_at IS NOT NULL) " +
            "OR (:isDeleted = FALSE AND deleted_at IS NULL)", nativeQuery = true)
    List<UserEntity> findAll(@Param("isEnabled") final boolean isEnabled, @Param("isDeleted") final boolean isDeleted);

    @Query(value = "SELECT * FROM users WHERE confirm_registration_token = :confirmRegistrationToken AND deleted_at IS NULL", nativeQuery = true)
    Optional<UserEntity> findByConfirmRegistrationToken(@Param("confirmRegistrationToken") final String confirmRegistrationToken);
}
