package com.example.answersboxapi.user.service.impl;

import com.example.answersboxapi.auth.service.MailService;
import com.example.answersboxapi.exceptions.AccessDeniedException;
import com.example.answersboxapi.exceptions.EntityNotFoundException;
import com.example.answersboxapi.exceptions.InvalidInputDataException;
import com.example.answersboxapi.exceptions.UnauthorizedException;
import com.example.answersboxapi.generated.model.*;
import com.example.answersboxapi.user.entity.UserEntity;
import com.example.answersboxapi.user.entity.enums.UserEntityRole;
import com.example.answersboxapi.user.model.UserDetailsImpl;
import com.example.answersboxapi.user.repository.UserRepository;
import com.example.answersboxapi.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static com.example.answersboxapi.user.mapper.UserMapper.USER_MAPPER;
import static com.example.answersboxapi.utils.MailUtils.createContext;
import static com.example.answersboxapi.utils.SecurityUtils.getCurrentUser;
import static com.example.answersboxapi.utils.SecurityUtils.isAdmin;
import static java.lang.String.format;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final MailService mailService;

    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void create(final SignUpRequest requestUser) {

        final String confirmRegistrationToken = randomAlphanumeric(15);

        final UserEntity userToSave = UserEntity.builder()
                .firstName(requestUser.getFirstName())
                .lastName(requestUser.getLastName())
                .email(requestUser.getEmail())
                .password(passwordEncoder.encode(requestUser.getPassword()))
                .isEnabled(false)
                .confirmRegistrationToken(confirmRegistrationToken)
                .confirmRegistrationDate(Instant.now())
                .createdAt(Instant.now())
                .role(UserEntityRole.ROLE_USER)
                .build();

        final Context context = createContext("confirmRegistrationToken", confirmRegistrationToken);
        mailService.sendEmail(requestUser.getEmail(), "confirm-registration.html", context);

        USER_MAPPER.toModel(userRepository.saveAndFlush(userToSave));
    }

    @Override
    @Transactional(readOnly = true)
    public boolean existByEmail(final String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public User getCurrent() {
        final UserDetailsImpl userDetails = getCurrentUser();

        return USER_MAPPER.toModel(
                userRepository.findByEmail(userDetails.getEmail(), true).orElseThrow(
                        () -> new EntityNotFoundException(String.format("User with email %s not found", userDetails.getEmail()))));
    }

    @Override
    public void deleteById(final UUID id) {
        if (!existById(id)) {
            throw new EntityNotFoundException(String.format("User with id: %s doesnt found", id));
        }
        checkAccess(id);
        userRepository.deleteById(id);
    }

    @Override
    public void changePassword(final ChangePasswordRequest changePasswordRequest) {
        final User currentUser = getCurrent();

        validateOldPassword(changePasswordRequest.getOldPassword(), currentUser.getPassword());

        final String encodedPassword = passwordEncoder.encode(changePasswordRequest.getNewPassword());
        currentUser.setPassword(encodedPassword);
        currentUser.setUpdatedAt(Instant.now());

        userRepository.saveAndFlush(USER_MAPPER.toEntity(currentUser));
    }

    @Override
    public void save(final User user) {
        userRepository.saveAndFlush(USER_MAPPER.toEntity(user));
    }

    @Override
    public User getUserByEmail(final String email) {
        return USER_MAPPER.toModel(userRepository.findByEmail(email, true).orElseThrow(
                () -> new EntityNotFoundException(format("User with email: %s not found", email))));
    }

    @Override
    public User getByPasswordResetToken(final String passwordResetToken) {
        return USER_MAPPER.toModel(userRepository.findByPasswordResetToken(passwordResetToken).orElseThrow(
                () -> new EntityNotFoundException(format("User with password reset token: %s not found", passwordResetToken))));
    }

    @Override
    public List<User> getAll(final boolean isEnabled, final boolean isDeleted) {
        if(!isAdmin()){
            throw new AccessDeniedException("Only admins can get deleted or inactive users");
        }
        return USER_MAPPER.toModelList(userRepository.findAll(isEnabled, isDeleted));
    }

    @Override
    public User getByConfirmationToken(final String confirmRegistrationToken) {
        return USER_MAPPER.toModel(userRepository.findByConfirmRegistrationToken(confirmRegistrationToken).orElseThrow(
                () -> new EntityNotFoundException(format("User with confirmation token: %s not found", confirmRegistrationToken))));
    }

    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        final UserEntity foundUser =
                userRepository.findByEmail(email, true).orElseThrow(() -> new UnauthorizedException("Email or password is invalid"));

        final UserDetailsImpl userDetails = new UserDetailsImpl();
        userDetails.setEmail(foundUser.getEmail());
        userDetails.setPassword(foundUser.getPassword());
        userDetails.setRole(foundUser.getRole());

        return userDetails;
    }

    private void validateOldPassword(final String requestedPassword, final String currentPassword) {
        if (!passwordEncoder.matches(requestedPassword, currentPassword)) {
            throw new InvalidInputDataException("Passwords don`t match");
        }
    }

    private void checkAccess(final UUID id) {
        final User currentUser = getCurrent();
        if (!(currentUser.getId().equals(id) || isAdmin())) {
            throw new AccessDeniedException(String.format("Low access to delete user with id: %s", id));
        }
    }

    private boolean existById(final UUID id) {
        return userRepository.existsById(id);
    }
}
