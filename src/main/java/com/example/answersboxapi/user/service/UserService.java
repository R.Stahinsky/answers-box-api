package com.example.answersboxapi.user.service;

import com.example.answersboxapi.generated.model.*;
import com.example.answersboxapi.user.entity.UserEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.UUID;

public interface UserService extends UserDetailsService {

    void create(final SignUpRequest requestUser);

    boolean existByEmail(final String email);

    User getCurrent();

    void deleteById(final UUID id);

    void changePassword(final ChangePasswordRequest changePasswordRequest);

    void save(final User user);

    User getUserByEmail(final String email);

    User getByPasswordResetToken(final String passwordResetToken);

    List<User> getAll(final boolean isEnabled, final boolean isDeleted);

    User getByConfirmationToken(final String confirmRegistrationToken);
}
