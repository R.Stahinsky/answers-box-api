package com.example.answersboxapi.answer.api;

import com.example.answersboxapi.answer.service.AnswerService;
import com.example.answersboxapi.generated.api.AnswersApi;
import com.example.answersboxapi.generated.model.Answer;
import com.example.answersboxapi.generated.model.AnswerRequest;
import com.example.answersboxapi.generated.model.AnswerUpdateRequest;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@Api(tags = "answers")
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class AnswerApiImpl implements AnswersApi {

    private final AnswerService answerService;

    @Override
    public ResponseEntity<Answer> createAnswer(@RequestBody final AnswerRequest answerRequest) {
        return new ResponseEntity<>(answerService.create(answerRequest), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Answer> updateAnswer(@PathVariable final UUID id, @RequestBody final AnswerUpdateRequest answerUpdateRequest) {
        return new ResponseEntity<>(answerService.updateById(id, answerUpdateRequest), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteAnswerById(@PathVariable final UUID id) {
        answerService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<Answer> increaseAnswerRating(@PathVariable final UUID id) {
        return new ResponseEntity<>(answerService.increaseRatingById(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Answer> decreaseAnswerRating(@PathVariable final UUID id) {
        return new ResponseEntity<>(answerService.decreaseRatingById(id), HttpStatus.OK);
    }
}
