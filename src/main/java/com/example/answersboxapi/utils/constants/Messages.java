package com.example.answersboxapi.utils.constants;

public class Messages {

    public static final String INVALID_EMAIL_MESSAGE = "must match \"^[a-z0-9._]+@[a-z0-9.-]+\\.[a-z]{2,6}$\"";
    public static final String INVALID_PASSWORD_MESSAGE =
            "must match \"(?=.*\\d)[\\w]{6,}\"";
}
