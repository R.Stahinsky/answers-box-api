package com.example.answersboxapi.utils;

import lombok.experimental.UtilityClass;
import org.thymeleaf.context.Context;

@UtilityClass
public class MailUtils {

    public static Context createContext(final String key, final String passwordResetToken){
        var context = new Context();
        context.setVariable(key, passwordResetToken);
        return context;
    }
}
