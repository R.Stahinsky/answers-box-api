package com.example.answersboxapi.exceptions;

import lombok.Data;

@Data
public class UnexpectedException {

    private String message;
}
