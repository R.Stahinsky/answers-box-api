package com.example.answersboxapi.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

public class TokenExpiredException extends RuntimeException{

    @Getter
    private final HttpStatus status;

    public TokenExpiredException(final String message) {
        super(message);
        this.status = HttpStatus.UNPROCESSABLE_ENTITY;
    }
}
