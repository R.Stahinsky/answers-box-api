package com.example.answersboxapi.auth.api;

import com.example.answersboxapi.auth.service.AuthService;
import com.example.answersboxapi.generated.api.AuthApi;
import com.example.answersboxapi.generated.model.*;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@Api(tags = "auth")
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class AuthApiImpl implements AuthApi {

    private final AuthService authService;

    @Override
    public ResponseEntity<Void> signUp(@Valid @RequestBody final SignUpRequest requestUser) {
        authService.signUp(requestUser);
        return new ResponseEntity<>(CREATED);
    }

    @Override
    public ResponseEntity<TokenResponse> signIn(@Valid @RequestBody final SignInRequest requestUser) {
        return new ResponseEntity<>(authService.signIn(requestUser), CREATED);
    }

    @Override
    public ResponseEntity<Void> logout() {
        authService.logout();
        return new ResponseEntity<>(OK);
    }

    @Override
    public ResponseEntity<Void> initResetPassword(@RequestBody final InitPasswordResetRequest initPasswordResetRequest) {
        authService.initResetPassword(initPasswordResetRequest);
        return new ResponseEntity<>(OK);
    }

    @Override
    public ResponseEntity<Void> finishResetPassword(@RequestBody final FinishPasswordResetRequest finishPasswordResetRequest) {
        authService.finishResetPassword(finishPasswordResetRequest);
        return new ResponseEntity<>(OK);
    }

    @Override
    public ResponseEntity<Void> confirmRegistration(@RequestBody final ConfirmRegistrationRequest confirmRegistrationRequest) {
        authService.confirmRegistration(confirmRegistrationRequest);
        return new ResponseEntity<>(OK);
    }
}
