package com.example.answersboxapi.auth.service.impl;

import com.example.answersboxapi.auth.service.AuthService;
import com.example.answersboxapi.auth.service.MailService;
import com.example.answersboxapi.config.JwtTokenProvider;
import com.example.answersboxapi.exceptions.EntityAlreadyProcessedException;
import com.example.answersboxapi.exceptions.EntityNotFoundException;
import com.example.answersboxapi.exceptions.TokenExpiredException;
import com.example.answersboxapi.exceptions.UnauthorizedException;
import com.example.answersboxapi.generated.model.*;
import com.example.answersboxapi.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import java.time.Instant;

import static com.example.answersboxapi.utils.MailUtils.createContext;
import static java.lang.String.format;
import static java.time.temporal.ChronoUnit.HOURS;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final MailService mailService;
    private final UserService userService;

    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    private final PasswordEncoder passwordEncoder;

    @Override
    public void signUp(final SignUpRequest requestUser) {
        if (userService.existByEmail(requestUser.getEmail())) {
            throw new EntityAlreadyProcessedException(format("User with email %s already exist", requestUser.getEmail()));
        }
        userService.create(requestUser);
    }

    @Override
    public TokenResponse signIn(final SignInRequest requestUser) {
        final Authentication authentication =
                authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(requestUser.getEmail(), requestUser.getPassword()));
        if (authentication == null) {
            throw new UnauthorizedException("Email or password is invalid");
        }
        return jwtTokenProvider.createToken(authentication);
    }

    @Override
    public void logout() {
        final User currentUser = userService.getCurrent();

        if (currentUser != null) {
            SecurityContextHolder.clearContext();
        } else throw new EntityNotFoundException("User not found");
    }

    @Override
    @Transactional
    public void initResetPassword(final InitPasswordResetRequest initPasswordResetRequest) {
        final User foundUser = userService.getUserByEmail(initPasswordResetRequest.getEmail());

        final String passwordResetToken = randomAlphanumeric(15);

        foundUser.setPasswordResetDate(Instant.now());
        foundUser.setPasswordResetToken(passwordResetToken);
        foundUser.setUpdatedAt(Instant.now());
        foundUser.setIsEnabled(false);
        userService.save(foundUser);

        final Context context = createContext("passwordResetToken", passwordResetToken);
        mailService.sendEmail(initPasswordResetRequest.getEmail(), "reset-password.html", context);
    }

    @Override
    @Transactional
    public void finishResetPassword(final FinishPasswordResetRequest finishPasswordResetRequest) {
        final User foundUser =
                userService.getByPasswordResetToken(finishPasswordResetRequest.getPasswordResetToken());

        isTokenExpired(foundUser.getPasswordResetDate());

        final String encodePassword = passwordEncoder.encode(finishPasswordResetRequest.getNewPassword());
        foundUser.setPassword(encodePassword);
        foundUser.setUpdatedAt(Instant.now());
        foundUser.setIsEnabled(true);
        userService.save(foundUser);
    }

    @Override
    public void confirmRegistration(final ConfirmRegistrationRequest confirmRegistrationRequest) {
        final User foundUser = userService.getByConfirmationToken(confirmRegistrationRequest.getConfirmRegistrationToken());

        isTokenExpired(foundUser.getConfirmRegistrationDate());

        foundUser.setIsEnabled(true);
        foundUser.setUpdatedAt(Instant.now());
        foundUser.setConfirmRegistrationDate(Instant.now());

        userService.save(foundUser);
    }

    private void isTokenExpired(final Instant dateToCheck) {
        if (dateToCheck.isBefore(Instant.now().minus(2, HOURS))) {
            throw new TokenExpiredException("Token is expired more than 2 hours.");
        }
    }
}
