package com.example.answersboxapi.auth.service;

import org.thymeleaf.context.Context;

public interface MailService {

    void sendEmail(final String email, final String templateLocation, final Context context);
}
