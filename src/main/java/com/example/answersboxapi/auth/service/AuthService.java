package com.example.answersboxapi.auth.service;


import com.example.answersboxapi.generated.model.*;

public interface AuthService {

    void signUp(final SignUpRequest signUpRequest);

    TokenResponse signIn(final SignInRequest signInRequest);

    void logout();

    void initResetPassword(final InitPasswordResetRequest initPasswordResetRequest);

    void finishResetPassword(final FinishPasswordResetRequest finishPasswordResetRequest);

    void confirmRegistration(final ConfirmRegistrationRequest confirmRegistrationRequest);
}
