package com.example.answersboxapi.auth.service.impl;

import com.example.answersboxapi.auth.service.MailService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.MimeMessage;

import static java.nio.charset.StandardCharsets.UTF_8;

@Service
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

    private static final String EMAIL_SUBJECT = "Answer-box API";

    private final JavaMailSender mailSender;
    private final TemplateEngine templateEngine;

    @Override
    @SneakyThrows
    public void sendEmail(final String email, final String templateLocation, final Context context) {

        final MimeMessage mail = mailSender.createMimeMessage();
        final String htmlTemplate = templateEngine.process(templateLocation, context);
        final MimeMessageHelper helper =
                new MimeMessageHelper(mail, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, UTF_8.name());

        helper.setTo(email);
        helper.setText(htmlTemplate, true);
        helper.setSubject(EMAIL_SUBJECT);

        mailSender.send(mail);
    }
}
