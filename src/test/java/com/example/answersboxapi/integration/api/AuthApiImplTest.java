package com.example.answersboxapi.integration.api;

import com.example.answersboxapi.generated.model.*;
import com.example.answersboxapi.integration.AbstractIntegrationTest;
import com.example.answersboxapi.user.entity.UserEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.ResultActions;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.stream.Stream;

import static com.example.answersboxapi.user.entity.enums.UserEntityRole.ROLE_USER;
import static com.example.answersboxapi.user.mapper.UserMapper.USER_MAPPER;
import static com.example.answersboxapi.utils.GeneratorUtil.*;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AuthApiImplTest extends AbstractIntegrationTest {

    @Test
    public void signUp_happyPath() throws Exception {
        //given
        final SignUpRequest signUpRequest = generateSignUpRequest();

        //when
        final ResultActions result = mockMvc.perform(post(AUTH_URL + "/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(signUpRequest)));

        //then
        result.andExpect(status().isCreated());
    }

    @Test
    public void signUp_whenEmailExist() throws Exception {
        //given
        final User savedUser = insertUser();

        final SignUpRequest signUpRequest = generateSignUpRequest(savedUser.getEmail(), savedUser.getPassword());

        //when
        final ResultActions resultAction = mockMvc.perform(post(AUTH_URL + "/sign-up")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(signUpRequest)));

        //then
        resultAction.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void signIn_happyPath() throws Exception {
        //given
        final SignUpRequest signUpRequest = generateSignUpRequest();
        insertUserOrAdmin(signUpRequest, ROLE_USER);

        //when
        final TokenResponse token = createSignIn(signUpRequest);

        //then
        assertNotNull(token);
    }

    @Test
    public void signIn_whenEmailInvalid() throws Exception {
        //given
        final User savedUser = insertUser();

        final SignInRequest signInRequest = generateInvalidSignInRequest();
        signInRequest.setPassword(savedUser.getPassword());

        //when
        final ResultActions result = mockMvc.perform(post(AUTH_URL + "/sign-in")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(signInRequest)));

        //then
        result.andExpect(status().isBadRequest());
    }

    @Test
    public void signIn_wrongUser() throws Exception {
        //given
        final SignInRequest signInRequest =
                generateSignInRequest(generateSignUpRequest().getEmail(), generateSignUpRequest().getPassword());

        //when
        final ResultActions result = mockMvc.perform(post(AUTH_URL + "/sign-in")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(signInRequest)));

        //then
        result.andExpect(status().isUnauthorized());
    }

    @Test
    public void logout_happyPath() throws Exception {
        //given
        final SignUpRequest signUpRequest = generateSignUpRequest();
        insertUserOrAdmin(signUpRequest, ROLE_USER);

        final TokenResponse token = createSignIn(signUpRequest);
        final TokenRequest tokenRequest = generateTokenRequest(token.getAccessToken());

        //when
        mockMvc.perform(post(AUTH_URL + "/logout")
                        .header(AUTHORIZATION,TOKEN_PREFIX + token.getAccessToken())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(tokenRequest)))
                        .andExpect(status().isOk())
                        .andReturn();

        //then
        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    public void logout_whenNotSignedIn() throws Exception {
        //given
        final SignUpRequest signUpRequest = generateSignUpRequest();
        insertUserOrAdmin(signUpRequest, ROLE_USER);

        final TokenResponse token = createSignIn(signUpRequest);
        final TokenRequest tokenRequest = generateTokenRequest(token.getAccessToken());

        //when
        final ResultActions result = mockMvc.perform(post(AUTH_URL + "/logout")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(tokenRequest)));

        //then
        result.andExpect(status().isUnauthorized());
    }

    @ParameterizedTest
    @MethodSource("initResetPasswordWithStatuses")
    public void initResetPassword(final HttpStatus status, final String email) throws Exception {
        //given
        final SignUpRequest signUpRequest = generateSignUpRequest();
        insertUserOrAdmin(signUpRequest, ROLE_USER);

        final InitPasswordResetRequest passwordResetRequest = new InitPasswordResetRequest();

        if (!status.equals(HttpStatus.OK)){
            signUpRequest.setEmail(email);
        }
        passwordResetRequest.setEmail(signUpRequest.getEmail());

        //when
        final ResultActions result = mockMvc.perform(post(AUTH_URL + "/reset-password/init")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(passwordResetRequest)));

        //then
        result.andExpect(status().is(status.value()));
    }

    @ParameterizedTest
    @MethodSource("finishResetPasswordWithStatuses")
    public void finishResetPassword(final HttpStatus status) throws Exception {
        //given
        final SignUpRequest signUpRequest = generateSignUpRequest();
        final User savedUser = insertUserOrAdmin(signUpRequest, ROLE_USER);

        final InitPasswordResetRequest passwordResetRequest = generateInitPasswordResetRequest(savedUser.getEmail());
        initResetPassword(passwordResetRequest);

        final UserEntity resetPasswordUser = userRepository.findByEmail(savedUser.getEmail(), false).get();
        final FinishPasswordResetRequest finishPasswordResetRequest =
                generateFinishPasswordResetRequest(savedUser.getEmail(), resetPasswordUser.getPasswordResetToken());

        if (!status.equals(HttpStatus.OK)) {
            resetPasswordUser.setPasswordResetDate(Instant.now().minus(3, ChronoUnit.HOURS));
            userRepository.saveAndFlush(resetPasswordUser);
        }
        if (status.equals(HttpStatus.NOT_FOUND)) {
            finishPasswordResetRequest.setPasswordResetToken(INVALID_TOKEN);
        }

        //when
        final ResultActions result = mockMvc.perform(put(AUTH_URL + "/reset-password/finish")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(finishPasswordResetRequest)));

        //then
        result.andExpect(status().is(status.value()));
    }

    @ParameterizedTest
    @MethodSource("confirmRegistrationWithStatuses")
    public void confirmRegistration(final HttpStatus status) throws Exception {
        //given
        final SignUpRequest signUpRequest = generateSignUpRequest();
        final User savedUser = insertUserOrAdmin(signUpRequest, ROLE_USER);

        final ConfirmRegistrationRequest confirmRegistrationRequest =
                generateConfirmRequest(savedUser.getConfirmRegistrationToken());

        if (!status.equals(HttpStatus.OK)) {
            savedUser.setConfirmRegistrationDate(Instant.now().minus(3,ChronoUnit.HOURS));
            userRepository.saveAndFlush(USER_MAPPER.toEntity(savedUser));
        }

        if (status.equals(HttpStatus.NOT_FOUND)) {
            confirmRegistrationRequest.setConfirmRegistrationToken(randomAlphanumeric(15));
        }

        //when
        final ResultActions result = mockMvc.perform(put(AUTH_URL + "/confirm-registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(confirmRegistrationRequest)));

        //then
        result.andExpect(status().is(status.value()));
    }

    static Stream<Arguments> initResetPasswordWithStatuses() {
        return Stream.of(
                arguments(HttpStatus.OK, ""),
                arguments(HttpStatus.BAD_REQUEST, INVALID_EMAIL),
                arguments(HttpStatus.NOT_FOUND, NOT_EXISTING_EMAIL)
        );
    }

    static Stream<Arguments> finishResetPasswordWithStatuses() {
        return Stream.of(
                arguments(HttpStatus.OK),
                arguments(HttpStatus.UNPROCESSABLE_ENTITY),
                arguments(HttpStatus.NOT_FOUND)
        );
    }

    static Stream<Arguments> confirmRegistrationWithStatuses() {
        return Stream.of(
                arguments(HttpStatus.OK),
                arguments(HttpStatus.UNPROCESSABLE_ENTITY),
                arguments(HttpStatus.NOT_FOUND));
    }

    private void initResetPassword(final InitPasswordResetRequest passwordResetRequest) throws Exception {
        mockMvc.perform(post(AUTH_URL + "/reset-password/init")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(passwordResetRequest)))
                .andExpect(status().isOk());
    }
}
