package com.example.answersboxapi.integration.api;

import com.example.answersboxapi.generated.model.ChangePasswordRequest;
import com.example.answersboxapi.generated.model.SignUpRequest;
import com.example.answersboxapi.generated.model.TokenResponse;
import com.example.answersboxapi.integration.AbstractIntegrationTest;
import com.example.answersboxapi.user.entity.enums.UserEntityRole;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.UUID;
import java.util.stream.Stream;

import static com.example.answersboxapi.user.entity.enums.UserEntityRole.ROLE_ADMIN;
import static com.example.answersboxapi.user.entity.enums.UserEntityRole.ROLE_USER;
import static com.example.answersboxapi.utils.GeneratorUtil.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserApiImplTest extends AbstractIntegrationTest {

    @ParameterizedTest
    @MethodSource("deleteWithStatusesAndRoles")
    public void deleteUser(final ResultMatcher status, final UserEntityRole role, UUID id, final boolean isCreator) throws Exception {
        //given
        final SignUpRequest usersRequest = generateSignUpRequest();
        insertUserOrAdmin(usersRequest, role);
        TokenResponse activeToken = createSignIn(usersRequest);

        final UUID idForSearch = checkIdForSearch(id, savedUser.getId());

        activeToken = isCreator(isCreator, activeToken, token);

        //when
        final ResultActions result = mockMvc.perform(delete(USER_URL + "/{id}", idForSearch)
                .header(AUTHORIZATION, TOKEN_PREFIX + activeToken.getAccessToken()));

        //then
        result.andExpect(status);
    }

    @ParameterizedTest
    @MethodSource("changePasswordWithStatusesAndPasswords")
    public void changePassword(final ResultMatcher status, final String password, final boolean passwordIsValid) throws Exception {
        //given
        final SignUpRequest userRequest = generateSignUpRequest();
        insertUserOrAdmin(userRequest, ROLE_USER);

        final TokenResponse activeToken = createSignIn(userRequest);

        if (!passwordIsValid){
            userRequest.setPassword(password);
        }
        final ChangePasswordRequest passwordRequest = createPasswordRequest(password, userRequest);

        //when
        final ResultActions result = mockMvc.perform(put(USER_URL + "/change-password")
                .header(AUTHORIZATION, TOKEN_PREFIX + activeToken.getAccessToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(passwordRequest)));

        //then
        result.andExpect(status);
    }


    @Test
    public void change_whenUnauthorized() throws Exception {
        //given
        final SignUpRequest userRequest = generateSignUpRequest();
        insertUserOrAdmin(userRequest, ROLE_USER);

        createSignIn(userRequest);

        final ChangePasswordRequest passwordRequest = createPasswordRequest(PASSWORD, userRequest);

        //when
        final ResultActions result = mockMvc.perform(put(USER_URL + "/change-password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(passwordRequest)));

        //then
        result.andExpect(status().isUnauthorized());
    }

    @ParameterizedTest
    @MethodSource("getAllWithStatusesRolesAndFlags")
    public void getAll(final ResultMatcher status, final UserEntityRole role, final boolean isEnabled, final boolean isDeleted) throws Exception {
        //given
        final SignUpRequest admin = generateSignUpRequest();
        insertUserOrAdmin(admin, role);
        final TokenResponse adminsToken = createSignIn(admin);

        //when
        final ResultActions result = mockMvc.perform(get(USER_URL + "/all")
                        .header(AUTHORIZATION, TOKEN_PREFIX + adminsToken.getAccessToken())
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("isEnabled", String.valueOf(isEnabled))
                        .param("isDeleted", String.valueOf(isDeleted)));

        //then
        result.andExpect(status);
    }

    static Stream<Arguments> changePasswordWithStatusesAndPasswords(){
        return Stream.of(
                arguments(status().isOk(), PASSWORD, true),
                arguments(status().isBadRequest(), INVALID_PASSWORD, true),
                arguments(status().isBadRequest(), PASSWORD, false));
    }

    static Stream<Arguments> deleteWithStatusesAndRoles() {
        return Stream.of(
                arguments(status().isNoContent(), ROLE_USER,  null, true),
                arguments(status().isNoContent(), ROLE_ADMIN,  null, false),
                arguments(status().isNotFound(), ROLE_USER, UUID.randomUUID(), false),
                arguments(status().isForbidden(), ROLE_USER,  null, false));
    }

    static Stream<Arguments> getAllWithStatusesRolesAndFlags() {
        return Stream.of(
                arguments(status().isOk(), ROLE_ADMIN, "true", "true"),
                arguments(status().isOk(), ROLE_ADMIN, "false", "false"),
                arguments(status().isOk(), ROLE_ADMIN, "true", "false"),
                arguments(status().isOk(), ROLE_ADMIN, "false", "true"),
                arguments(status().isForbidden(), ROLE_USER, "true", "true"),
                arguments(status().isForbidden(), ROLE_USER, "false", "false"),
                arguments(status().isForbidden(), ROLE_USER, "true", "false"),
                arguments(status().isForbidden(), ROLE_USER, "false", "true")
        );
    }
}
