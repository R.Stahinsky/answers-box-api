package com.example.answersboxapi.utils;

import com.example.answersboxapi.generated.model.*;
import com.example.answersboxapi.user.entity.UserEntity;
import com.example.answersboxapi.user.entity.enums.UserEntityRole;
import com.github.javafaker.Faker;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

public class GeneratorUtil {

    private static final Faker FAKER = new Faker();

    public static final String INVALID_PASSWORD = FAKER.name().name();
    public static final String INVALID_EMAIL = "qwe.rty@gmailcom";
    public static final String INVALID_TOKEN = FAKER.name().name();
    public static final String NOT_EXISTING_EMAIL = "notexisting@gmail.com";
    public static final String PASSWORD = "qwerty123";


    public static UserEntity generateUser() {
        return UserEntity.builder()
                .id(UUID.randomUUID())
                .firstName(FAKER.name().firstName() + Math.random())
                .lastName(FAKER.name().lastName() + Math.random())
                .email(FAKER.internet().emailAddress())
                .password(FAKER.internet().password(true) + 1)
                .isEnabled(true)
                .createdAt(Instant.now().truncatedTo(ChronoUnit.SECONDS))
                .role(UserEntityRole.ROLE_USER)
                .build();
    }

    public static SignUpRequest generateSignUpRequest() {
        final SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setEmail(FAKER.internet().emailAddress());
        signUpRequest.setFirstName(FAKER.name().firstName() + Math.random());
        signUpRequest.setLastName(FAKER.name().lastName() + Math.random());
        signUpRequest.setPassword(FAKER.internet().password(true) + 1);

        return signUpRequest;
    }

    public static SignUpRequest generateSignUpRequest(final String email, final String password) {
        final SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setEmail(email);
        signUpRequest.setFirstName(FAKER.name().firstName() + Math.random());
        signUpRequest.setLastName(FAKER.name().lastName() + Math.random());
        signUpRequest.setPassword(password);

        return signUpRequest;
    }

    public static SignInRequest generateInvalidSignInRequest() {
        SignInRequest signInRequest = new SignInRequest();
        signInRequest.setEmail(INVALID_EMAIL);
        signInRequest.setPassword(INVALID_PASSWORD);

        return signInRequest;
    }

    public static SignInRequest generateSignInRequest(final String login, final String password) {
        final SignInRequest signInRequest = new SignInRequest();
        signInRequest.setEmail(login);
        signInRequest.setPassword(password);

        return signInRequest;
    }

    public static SignInRequest generateInvalidSignInRequest(final String email, final String password) {
        final SignInRequest signInRequest = new SignInRequest();
        signInRequest.setEmail(email);
        signInRequest.setPassword(password);

        return signInRequest;
    }

    public static TokenRequest generateTokenRequest(final String accessToken) {
        final TokenRequest tokenRequest = new TokenRequest();
        tokenRequest.setToken(accessToken);

        return tokenRequest;
    }

    public static TagRequest generateTagRequest() {
        final TagRequest tagRequest = new TagRequest();
        tagRequest.setName(FAKER.name().title() + Math.random());

        return tagRequest;
    }

    public static QuestionRequest generateQuestionRequest() {
        final QuestionRequest questionRequest = new QuestionRequest();
        questionRequest.title(FAKER.name().title() + Math.random());
        questionRequest.description(FAKER.name().name() + Math.random());

        return questionRequest;
    }

    public static QuestionRequest generateQuestionWithEmptyFields() {
        final QuestionRequest questionRequest = new QuestionRequest();
        questionRequest.title("");
        questionRequest.description("");

        return questionRequest;
    }

    public static AnswerRequest generateAnswerRequest() {
        final AnswerRequest answerRequest = new AnswerRequest();
        answerRequest.setText(FAKER.name().title() + Math.random());

        return answerRequest;
    }

    public static AnswerRequest generateEmptyAnswer() {
        final AnswerRequest answerRequest = new AnswerRequest();
        answerRequest.setText("");

        return answerRequest;
    }

    public static AnswerUpdateRequest generateAnswerUpdateRequest() {
        final AnswerUpdateRequest answerUpdateRequest = new AnswerUpdateRequest();
        answerUpdateRequest.setText(FAKER.name().title() + Math.random());

        return answerUpdateRequest;
    }

    public static QuestionUpdateRequest generateQuestionUpdateRequest() {
        final QuestionUpdateRequest questionUpdateRequest = new QuestionUpdateRequest();
        questionUpdateRequest.title(FAKER.name().title() + Math.random());
        questionUpdateRequest.description(FAKER.name().name() + Math.random());

        return questionUpdateRequest;
    }

    public static QuestionUpdateRequest generateQuestionUpdateWithEmptyFields() {
        final QuestionUpdateRequest questionUpdateRequest = new QuestionUpdateRequest();
        questionUpdateRequest.title("");
        questionUpdateRequest.description("");

        return questionUpdateRequest;
    }

    public static InitPasswordResetRequest generateInitPasswordResetRequest(final String email) {
        final InitPasswordResetRequest passwordResetRequest = new InitPasswordResetRequest();
        passwordResetRequest.setEmail(email);

        return passwordResetRequest;
    }

    public static FinishPasswordResetRequest generateFinishPasswordResetRequest(final String email, final String resetPasswordToken) {
        final FinishPasswordResetRequest finishPasswordResetRequest = new FinishPasswordResetRequest();
        finishPasswordResetRequest.setEmail(email);
        finishPasswordResetRequest.setPasswordResetToken(resetPasswordToken);
        finishPasswordResetRequest.setNewPassword(FAKER.internet().password() + 1);

        return finishPasswordResetRequest;
    }

    public static ConfirmRegistrationRequest generateConfirmRequest(final String token) {
        final ConfirmRegistrationRequest confirmRequest = new ConfirmRegistrationRequest();
        confirmRequest.setConfirmRegistrationToken(token);

        return confirmRequest;
    }
}
