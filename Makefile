clean:
	gradlew clean

build: clean
ifdef skip-tests
	gradlew build -x test
else
	gradlew build
endif

run-docker-app: build
	docker-compose up

newrelic-local:
	java -jar .\build\libs\answers-box-api-0.0.1-SNAPSHOT.jar -javaagent:.\newrelic\newrelic.jar --spring.profiles.active=local
